"""garments_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from main.views import ExportDocViewSet, AcceptedBillsViewSet, BTBLCViewSet, LCAccountViewSet, LCAmndMergeViewSet, \
    MasterLCViewset, ReportViewSet


router = routers.DefaultRouter()
router.register(r'exportdoc', ExportDocViewSet, 'ExportDoc')
router.register(r'acceptedbills', AcceptedBillsViewSet, 'AcceptedBills')
router.register(r'btblc', BTBLCViewSet, 'BTBLC')
router.register(r'lcaccount', LCAccountViewSet, 'LCAccount')
router.register(r'lcamndmerge', LCAmndMergeViewSet, 'LCAmndMerge')
router.register(r'masterlc', MasterLCViewset, 'MasterLC')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path(r'api/accounts/', include('authemail.urls')),
    path(r'report/', ReportViewSet.as_view())
]
