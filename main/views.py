from main.models import CustomUser, ExportDoc,AcceptedBills, BTBLC, LCAccount, LCAmndMerge, MasterLC
from main.serializers import AcceptedBillsSerializer, ExportDocSerializer, LCAccountSerializer, BTBLCSerializer, LCAmndMergeSerializer, MasterLCSerializer
from rest_framework import viewsets
from django.core.exceptions import ImproperlyConfigured
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import authentication
from django.http import HttpResponse, JsonResponse
from django.db.models import Sum



class MasterLCViewset(viewsets.ModelViewSet):
    queryset = MasterLC.objects.all()
    serializer_class = MasterLCSerializer


class LCAmndMergeViewSet(viewsets.ModelViewSet):
    def get_queryset(self):

        master_lc = self.request.query_params.get('mlc_id', None)
        queryset = LCAmndMerge.objects.all()

        if master_lc is not None:
            queryset = queryset.filter(mlc_id=master_lc)

        return queryset

    serializer_class = LCAmndMergeSerializer


class LCAccountViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        master_lc = self.request.query_params.get('mlc_id', None)
        queryset = LCAccount.objects.all()

        if master_lc is not None:
            queryset = queryset.filter(mlc_id=master_lc)
        return queryset
    serializer_class = LCAccountSerializer


class BTBLCViewSet(viewsets.ModelViewSet):

    def get_queryset(self):
        master_lc = self.request.query_params.get('mlc_id', None)
        queryset = BTBLC.objects.all()

        if master_lc is not None:
            queryset = queryset.filter(mlc_id=master_lc)
        return queryset

    serializer_class = BTBLCSerializer


class AcceptedBillsViewSet(viewsets.ModelViewSet):

    def get_queryset(self):

        master_lc = self.request.query_params.get('mlc_id', None)
        btb_lc_id = self.request.query_params.get('btb_lc_id', None)
        queryset = AcceptedBills.objects.all()

        if btb_lc_id is not None:
            queryset = queryset.filter(btb_lc_id=btb_lc_id)
        if master_lc is not None:
            queryset = queryset.filter(btb_lc_id__mlc_id=master_lc)

        return queryset

    serializer_class = AcceptedBillsSerializer


class ExportDocViewSet(viewsets.ModelViewSet):

    def get_queryset(self):
        master_lc = self.request.query_params.get('mlc_id', None)
        queryset = ExportDoc.objects.all()

        if master_lc is not None:
            queryset = queryset.filter(mlc_id=master_lc)
        return queryset
    serializer_class = ExportDocSerializer


class ReportViewSet(APIView):
    authentication_classes = (authentication.TokenAuthentication,)

    def get(self, request, format=None):
        mlc_id = self.request.query_params.get('mlc_id', None)

        if request.user.is_authenticated:
            results = []
            if mlc_id is not None:
                master_lcs = MasterLC.objects.filter(mlc_id=mlc_id)
            else:
                master_lcs = MasterLC.objects.all()
            final_result = []
            for master_lc in master_lcs:
                result = dict()
                result['mlc_id'] = master_lc.mlc_id
                result['buying_house'] = master_lc.buying_house # column 1
                lc_ammend_merge_list = LCAmndMerge.objects.filter(mlc_id=master_lc.mlc_id)
                lc_ammend_quantity = 0
                lc_ammend_value = 0
                value_after_commission_value = 0
                summation_btb_lc_limit = 0
                for lc_ammend_merge in lc_ammend_merge_list:
                    lc_ammend_quantity += lc_ammend_merge.quantity
                    lc_ammend_value += lc_ammend_merge.lc_value
                    after_commission = float(lc_ammend_merge.lc_value) - 0.01 * lc_ammend_merge.commission
                    btb_lc_limit = after_commission * 0.01 * lc_ammend_merge.limit
                    summation_btb_lc_limit += btb_lc_limit
                    value_after_commission_value += after_commission

                result['lc_ammend_quantity'] = lc_ammend_quantity # column 2
                result['lc_ammend_value'] = lc_ammend_value # column 3
                result['value_after_commission'] = round(value_after_commission_value, 2) # column 4
                result['btb_lc_limit'] = round(summation_btb_lc_limit, 2) # column 5

                btblc_list = BTBLC.objects.filter(mlc_id=master_lc)
                btb_lc_opened = btblc_list.aggregate(Sum('lc_value'))

                result['btb_lc_opened'] = btb_lc_opened['lc_value__sum'] if btblc_list else 0 # column 6
                result['btb_available'] = round(summation_btb_lc_limit - float(result['btb_lc_opened']), 2) # column 7
                result['pc_amount'] = round(value_after_commission_value * 0.10, 2) # column 8

                lc_account_list = LCAccount.objects.filter(mlc_id=master_lc)

                pc_list = lc_account_list.filter(withdraw_type="PC")
                pc_drawn = pc_list.aggregate(Sum('amount'))

                cd_list = lc_account_list.filter(withdraw_type="CD")
                cd_drawn = cd_list.aggregate(Sum('amount'))

                result['pc_drawn'] = round(pc_drawn['amount__sum'], 2) if pc_list else 0 # Column 9
                result['available_pc'] = round(result['pc_amount'] - float(result['pc_drawn']), 2) # column 10

                export_doc_list = ExportDoc.objects.filter(mlc_id=master_lc)
                export_doc_value = export_doc_list.aggregate(Sum('value'))

                result['export_doc_value'] = export_doc_value['value__sum'] if export_doc_list else 0 # column 11
                result['cd_amount'] = round(0.10 * float(result['export_doc_value']), 2) # column 12
                result['cd_drawn'] = cd_drawn['amount__sum'] if cd_list else 0 # column 13
                result['available_cd'] = round(result['cd_amount'] - float(result['cd_drawn']), 2)

                final_result.append(result)

            return JsonResponse(data={"success": "true",
                                      "response": final_result}, status=status.HTTP_200_OK)
