from rest_framework import serializers
from main.models import CustomUser, ExportDoc,AcceptedBills, BTBLC, LCAccount, LCAmndMerge, MasterLC


class ExportDocSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExportDoc
        fields = (
            'id',
            'mlc_id',
            'invoice_no',
            'exp_no',
            'delivery_date',
            'quantity',
            'cartoon_quantity',
            'value',
            'shipping_line',
            'notify_party',
            'buyer_or_local_agent',
            'copy_bl_rcv_date',
            'original_bl_rcv_date',
            'gsp_date',
            'ic_date',
            'doc_sent_date',
            'courier_no',
            'payment_term',
            'maturity_date',
            'payment_received',
            'payment_received_date',
            'realized_amount',
            'remarks'
        )


class AcceptedBillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AcceptedBills
        fields = (
            'id',
            'btb_lc_id',
            'invoice_no',
            'doc_value',
            'acceptance_date',
            'bank_acceptance_date',
            'maturity_date',
            'payment_date',
            'payment_value',
            'payment_method',
            'remarks'
        )


class BTBLCSerializer(serializers.ModelSerializer):
    class Meta:
        model = BTBLC
        fields = (
            'btb_lc_id',
            'mlc_id',
            'lc_no',
            'lc_value',
            'lc_open_date',
            'lc_value',
            'ship_date',
            'beneficiary',
            'payment_method',
            'remarks'
        )


class LCAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = LCAccount
        fields = (
            'id',
            'mlc_id',
            'withdraw_type',
            'voucher_no',
            'date',
            'amount',
            'remarks'
        )


class LCAmndMergeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LCAmndMerge
        fields = (
            'id',
            'mlc_id',
            'sc_no',
            'lc_no',
            'alauddin_ref',
            'shipment_date',
            'quantity',
            'lc_value',
            'payment_method',
            'amndmnt_no',
            'limit',
            'commission'
        )


class MasterLCSerializer(serializers.ModelSerializer):
    class Meta:
        model = MasterLC
        fields = (
            'mlc_id',
            'buyer',
            'buying_house',
            'lc_or_sc',
            'remarks'
        )