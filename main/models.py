from django.db import models
from authemail.models import EmailUserManager, EmailAbstractUser


class CustomUser(EmailAbstractUser):
    # Custom fields
    date_of_birth = models.DateField('Date of birth', null=True,
                                     blank=True)

    # Required
    objects = EmailUserManager()


class MasterLC(models.Model):
    LC_OR_SC_CHOICES = (
        ('LC', 'LC'),
        ('SC', 'SC'),
        ('LC/SC', 'LC/SC'),
    )
    mlc_id = models.AutoField(primary_key=True)
    buyer = models.CharField(max_length=100, blank=True, null=True)
    buying_house = models.CharField(max_length=100, blank=True, null=True)
    lc_or_sc = models.CharField(max_length=10, choices=LC_OR_SC_CHOICES, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['mlc_id']
        verbose_name = "MasterLC"
        verbose_name_plural = 'MasterLCs'

    def __str__(self):
        return '%s %s' % (self.mlc_id, self.buyer)


class LCAmndMerge(models.Model):

    mlc_id = models.ForeignKey('MasterLC', on_delete=models.CASCADE, blank=True, null=True)
    sc_no = models.CharField(max_length=20, blank=True, null=True)
    lc_no = models.CharField(max_length=20, blank=True, null=True)
    lc_value = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    payment_method = models.CharField(max_length=100, blank=True, null=True)
    alauddin_ref = models.CharField(max_length=20, blank=True, null=True)
    shipment_date = models.DateField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    amndmnt_no = models.IntegerField(blank=True, null=True)
    limit = models.IntegerField(blank=True, null=True)
    commission = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = "LC_AMND_MERGE"


class LCAccount(models.Model):

    WITHDRAW_TYPE_CHOICES = (
        ('PC', 'PC'),
        ('CD', 'CD'),
        ('OTHERS', 'OTHERS'),
    )

    mlc_id = models.ForeignKey('MasterLC', on_delete=models.CASCADE, blank=True, null=True)
    withdraw_type = models.CharField(max_length=10, choices=WITHDRAW_TYPE_CHOICES, blank=True, null=True)
    voucher_no = models.CharField(max_length=20, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)


class BTBLC(models.Model):

    btb_lc_id = models.AutoField(primary_key=True)
    mlc_id = models.ForeignKey('MasterLC', on_delete=models.CASCADE, blank=True, null=True)
    lc_no = models.CharField(max_length=20, blank=True, null=True)
    lc_open_date = models.DateField(blank=True, null=True)
    lc_value = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    beneficiary = models.CharField(max_length=100, blank=True, null=True)
    ship_date = models.DateField(blank=True, null=True)
    payment_method = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = "BTB_LC"

    def __str__(self):
        return '%s %s' % (self.btb_lc_id, self.mlc_id)


class AcceptedBills(models.Model):

    btb_lc_id = models.ForeignKey('BTBLC', on_delete=models.CASCADE, blank=True, null=True)
    invoice_no = models.CharField(max_length=20, blank=True, null=True)
    doc_value = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    acceptance_date = models.DateField(blank=True, null=True)
    bank_acceptance_date = models.DateField(blank=True, null=True)
    maturity_date = models.DateField(blank=True, null=True)
    payment_date = models.DateField(blank=True, null=True)
    payment_value = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    payment_method = models.CharField(max_length=100, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = "Accepted Bills"
        verbose_name_plural = 'Accepted Bills'


class ExportDoc(models.Model):

    mlc_id = models.ForeignKey('MasterLC', on_delete=models.CASCADE, blank=True, null=True)
    invoice_no = models.CharField(max_length=20, blank=True, null=True)
    exp_no = models.CharField(max_length=20, blank=True, null=True)
    delivery_date = models.DateField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    cartoon_quantity = models.IntegerField(blank=True, null=True)
    value = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    shipping_line = models.CharField(max_length=100, blank=True, null=True)
    notify_party = models.CharField(max_length=100, blank=True, null=True)
    buyer_or_local_agent = models.CharField(max_length=20, blank=True, null=True)
    copy_bl_rcv_date = models.DateField(blank=True, null=True)
    original_bl_rcv_date = models.DateField(blank=True, null=True)
    gsp_date = models.DateField(blank=True, null=True)
    ic_date = models.DateField(blank=True, null=True)
    doc_sent_date = models.DateField(blank=True, null=True)
    courier_no = models.CharField(max_length=20, blank=True, null=True)
    payment_term = models.CharField(max_length=20, blank=True, null=True)
    maturity_date = models.DateField(blank=True, null=True)
    payment_received = models.NullBooleanField(blank=True, null=True, default=False)
    payment_received_date = models.DateField(blank=True, null=True)
    realized_amount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)