from django.contrib import admin
from django.contrib.auth import get_user_model
from authemail.admin import EmailUserAdmin
from main.models import MasterLC, LCAmndMerge, LCAccount, BTBLC, AcceptedBills, ExportDoc, CustomUser


class CustomUserAdmin(EmailUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal Info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff',
                                       'is_superuser', 'is_verified',
                                       'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Custom info', {'fields': ('date_of_birth',)}),
    )


class MasterLCAdmin(admin.ModelAdmin):
    list_display = ('mlc_id', 'buyer', 'buying_house', 'lc_or_sc', 'remarks')

class LCAmndMergeAdmin(admin.ModelAdmin):
    list_display = ('mlc_id', 'sc_no', 'lc_no', 'alauddin_ref', 'shipment_date', 'quantity')

class LCAccountAdmin(admin.ModelAdmin):
    list_display = ('mlc_id', 'withdraw_type', 'voucher_no', 'date', 'amount')

class BTBLCAdmin(admin.ModelAdmin):
    list_display = ('mlc_id', 'lc_no', 'lc_open_date', 'lc_value', 'ship_date')

class AcceptedBillsAdmin(admin.ModelAdmin):
    list_display = ('btb_lc_id', 'invoice_no', 'doc_value', 'acceptance_date', 'payment_value')

class ExportDocAdmin(admin.ModelAdmin):
    list_display = ('mlc_id', 'invoice_no', 'delivery_date', 'quantity', 'realized_amount')


# admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), CustomUserAdmin)
admin.site.register(MasterLC, MasterLCAdmin)
admin.site.register(LCAmndMerge, LCAmndMergeAdmin)
admin.site.register(LCAccount, LCAccountAdmin)
admin.site.register(BTBLC, BTBLCAdmin)
admin.site.register(AcceptedBills, AcceptedBillsAdmin)
admin.site.register(ExportDoc, ExportDocAdmin)